% Michael Schmid

\section{Die Dirac-Gleichung}

Einige Probleme der Klein-Gordon-Gleichung lassen sich beseitigen,
wenn man eine Differentialgleichung erster Ordnung findet. Diese wurde
1928 von P.\ Dirac aufgestellt. Die Forderungen an diese Gleichung sind:
\begin{itemize}
\item relativistische Kovarianz,
\item nur Ableitungen erster Ordnung treten auf,
\item die Lösungen $\bm{\psi}$ der neuen Gleichung müssen auch die
  Klein-Gordon-Gleichung erfüllen,
\item die Gleichung muss eine positiv definite Wahrscheinlichkeitsdichte ergeben.
\end{itemize}

\begin{notice}
  Hier wird eine Argumentation zum Aufstellen der
  Dirac-Gleichung aufgeführt, wie sie etwa im Anhang des Lehrbuches von F.\ Schwabl
  (siehe Literaturverzeichnis) zu finden ist. Sie entspricht nicht dem üblichen Weg, der aber bereits Bestandteil der Kursvorlesungen ist.
\end{notice}

Ausgangspunkt der Argumentation sei das freie Teilchen in der
Schrödingergleichung
\begin{align}
  H &= \frac{\bm{p}^2}{2m} \mcomma
\end{align}
wobei wir die Relation
\begin{align}
  \label{eq:2014-10-21-1}
  (\bm{\sigma} \cdot \bm{a})(\bm{\sigma} \cdot \bm{b} )
  &= \bm{a}\cdot \bm{b} + \ii \bm{\sigma} (\bm{a} \times \bm{b})
\end{align}
zum Umschreiben des Hamiltonoperators verwenden. Der Vektor
$\bm{\sigma}$ besitzt die Pauli-Matrizen als Einträge, d.h.\
\begin{align*}
  \bm{\sigma} &=
  \begin{pmatrix}
    \sigma_x \\ \sigma_y \\\sigma_z
  \end{pmatrix} \mpunkt
\end{align*}
Man kann dann schreiben
\begin{align}
  H &= \frac{ (\bm{\sigma} \cdot \bm{p})(\bm{\sigma} \cdot \bm{p} )}{2m}
  \eqrel{eq:2014-10-21-1}{=} \frac{\bm{p}^2}{2m} + \ii \bm{\sigma}
  \underbrace{(\bm{p}\times \bm{p})}_{=0} \frac{1}{2m} = \frac{\bm{p}^2}{2m} \mpunkt
\end{align}
Nach dem Prinzip der minimalen Ankopplung erfolgt die Ersetzung
\begin{align}
  \nonumber
  \bm{p} &\to \bm{p} - e \bm{A} \mcomma \\
  H &\to H + e \phi
\end{align}
für ein geladenes Teilchen im elektromagnetischen Feld. Im gerade gefundenen Ansatz ergibt sich damit
\begin{align}
  \nonumber
  H &= \frac{1}{2m} \left[ \bm{\sigma}\cdot  (\bm{p} - e \bm{A})  \right]
  \left[ \bm{\sigma}\cdot (\bm{p} - e \bm{A})  \right] + e \phi \\
  \nonumber
  &\eqrel{eq:2014-10-21-1}{=} \frac{1}{2m} (\bm{p} - e \bm{A})^2 + \frac{\ii}{2m} \bm{\sigma}
    \left[ (\bm{p}- e \bm{A}) \times  (\bm{p}- e \bm{A}) \right] + e \phi \\
  &= \frac{1}{2m} (\bm{p} - e \bm{A})^2 - \frac{e \hbar}{2m} \bm{\sigma} \cdot \bm{B} + e \phi.
\end{align}
Dies ist die \acct{Pauli-Gleichung}, sie entspricht dem
nicht relativistischen Grenzfall der Dirac-Gleichung.

Ähnlich lässt sich die relativistische Energie-Impuls-Beziehung
behandeln.
\begin{align*}
  \left( \frac{E}{c} - \bm{\sigma} \cdot \bm{p}  \right)\left( \frac{E}{c}
    + \bm{\sigma} \cdot \bm{p}  \right)
  &= \frac{E^2}{c^2} - (\bm{\sigma}\cdot\bm{p})(\bm{\sigma}\cdot\bm{p}) \\
  &\eqrel{eq:2014-10-21-1}{=} \frac{E^2}{c^2} - \bm{p^2} \eqrel{eq:2014-10-14-6a}{=} m^2 c^2 \mpunkt
\end{align*}
Mit den Jordanschen Ersetzungsregeln erhält man
\begin{align}
  \bigg( \frac{\ii \hbar}{c} \partial_t + \bm{\sigma}\cdot \ii \hbar \nabla  \bigg)
  \underbrace{ \bigg( \frac{\ii \hbar}{c} \partial_t - \bm{\sigma}\cdot \ii \hbar \nabla  \bigg)
    \underbrace{ \varphi }_{-\varphi_1}}_{= -mc \varphi_2}
  &= m^2c^2 \underbrace{ \varphi }_{-\varphi_1} \mpunkt
\end{align}
Mit den beiden zweikomponentigen Spinoren $\varphi_1$ und $\varphi_2$
erhalten wir zwei gekoppelte Differentialgleichungen erster Ordnung,
\begin{subequations}
  \begin{align}
    \label{eq:2014-10-21-2a}
    \ii \hbar \left[ \frac{\partial}{\partial (ct)}
      - \bm{\sigma} \cdot \nabla  \right] \varphi_1 &= m c \varphi_2 \mcomma \\
    \label{eq:2014-10-21-2b}
    \ii \hbar \left[ \frac{\partial}{\partial (ct)}
      + \bm{\sigma} \cdot \nabla  \right] \varphi_2 &= m c \varphi_1 \mpunkt
  \end{align}
\end{subequations}
Mit der Schreibweise $\bm{\sigma}\cdot \nabla = \sigma^i \partial_i$
und $\partial/\partial(ct) = \partial_0$ folgt für
$\eqref{eq:2014-10-21-2b}-\eqref{eq:2014-10-21-2a}$ und
$-\eqref{eq:2014-10-21-2b}-\eqref{eq:2014-10-21-2a}$
\begin{subequations}
  \begin{align}
    \ii \hbar \left[ \partial_0 (\varphi_2-\varphi_1)
      + \sigma^i \partial_i (\varphi_2 + \varphi_1)  \right] &=  .mc (\varphi_2 -\varphi_1),\\
    - \ii \hbar \left[ \partial_0 (\varphi_2+\varphi_1)
      + \sigma^i \partial_i (\varphi_2 - \varphi_1)  \right] &= - mc (\varphi_2 +\varphi_1).
  \end{align}
\end{subequations}
Mit dem Vierer-Spinor
\begin{align}
  \bm{\psi} &=
  \begin{pmatrix}
    \varphi_2 - \varphi_1 \\ \varphi_2 + \varphi_1
  \end{pmatrix}
\end{align}
ergibt sich
\begin{align}
  \label{eq:2014-10-21-02}
  \ii \hbar \left[
    \begin{pmatrix}
      \mathds{1} & 0 \\ 0 & -\mathds{1} 
    \end{pmatrix} \partial_0+
    \begin{pmatrix}
      0         & \sigma^i \\ 
      -\sigma^i & 0
    \end{pmatrix} \partial_i - \frac{mc}{\ii \hbar} \right] \bm{\psi} &= 0
\end{align}
oder 
\begin{subequations}
  \begin{align}
    \nonumber
    \left[ \ii \gamma^\mu \partial_\mu - \frac{mc}{\hbar}  \right] \bm{\psi} &= 0
    \intertext{mit}
    \gamma^0 &=
    \begin{pmatrix}
      \mathds{1} & 0 \\ 0 & -\mathds{1}
    \end{pmatrix} \mcomma \\
    \gamma^i &=
    \begin{pmatrix}
      0 & \sigma^i \\ - \sigma^i & 0
    \end{pmatrix} \mpunkt
  \end{align}
\end{subequations}

Dies ist die \acct{Dirac-Gleichung für ein freies Teilchen}. Eine
andere Schreibweise ist
\begin{subequations}
  \begin{align}
    \label{eq:2014-10-21-3a} 
    \ii \hbar \partial_t \bm{\psi}
    &= \left( \frac{\hbar c}{\ii} \alpha^k \partial_k + \beta m c^2 \right) \bm{\psi}
    \intertext{mit}
    \nonumber
    \beta &=
    \begin{pmatrix}
      \mathds{1} & 0 \\ 0 & -\mathds{1}
    \end{pmatrix} \mcomma \\
    \alpha^i = \beta \gamma^i&=
    \begin{pmatrix}
      0        & \sigma^i \\ 
      \sigma^i & 0
    \end{pmatrix} \mpunkt
  \end{align}
\end{subequations}

Eine besonders kompakte Schreibweise ergibt sich unter Verwendung des
\acct{Feynman-Dagger Symbols}
\begin{align*}
  \slashed{\partial} &= \gamma^\mu \partial_\mu
  = \frac{\gamma^0}{c} \partial_t + \bm{\gamma} \nabla \\
  \slashed{\hat{p}} &= \gamma^\mu \hat{p}_\mu
  = \gamma^0 \hat{p}_0 + \gamma^i \hat{p}_i.
\end{align*}
Damit folgt also 
\begin{align}
  \left(\slashed{\hat{p}}  - mc  \right) \bm{\psi}(\bm{r},t) &= 0 \mcomma  \label{eq:136} \\
  \left( \ii \slashed{\partial} - \frac{mc}{\hbar}  \right) \bm{\psi}(\bm{r},t) &= 0  \mpunkt \label{eq:137}
\end{align}

\subsection{Kontinuitätsgleichung}

Multipliziere Gleichung~\eqref{eq:2014-10-21-3a} von Links mit
$\bm{\psi}^\dagger = \left( \psi_1^\ast , \psi_2^\ast, \psi_3^\ast,
  \psi_4^\ast \right)$
\begin{subequations}
  \begin{align}
    \label{eq:2014-10-21-4a}
    \ii \hbar \bm{\psi}^\dagger \partial_t \bm{\psi}
    &= \frac{\hbar c}{\ii} \left( \bm{\psi}^\dagger \alpha^k \partial_k \bm{\psi}
      + m c^2 \bm{\psi}^\dagger \beta \bm{\psi}  \right)
    \intertext{Die dazu konjugiert-komplexe Relation ist:}
    \label{eq:2014-10-21-4b}
    - \ii \hbar \frac{\partial \bm{\psi}^\dagger}{\partial t } \bm{\psi}
    &= - \frac{\hbar c}{\ii} \left( \partial_i \bm{\psi}^\dagger \right) {\alpha^i}^\dagger \bm{\psi}
    + mc^2 \bm{\psi}^\dagger  \beta \bm{\psi} \mpunkt
  \end{align}
\end{subequations}
Betrachte $\eqref{eq:2014-10-21-4b} - \eqref{eq:2014-10-21-4a}$
\begin{align}
  \partial_t \bm{\psi}^\dagger \bm{\psi}
  &= - c (\partial_i \bm{\psi}^\dagger) {\alpha^i}^\dagger \bm{\psi}
  + c \bm{\psi}^\dagger \alpha^i \partial_i \bm{\psi}
  + \frac{\ii m c^2}{\hbar} \left( \bm{\psi}^\dagger \beta^\dagger \bm{\psi}
    - \bm{\psi}^\dagger \beta \bm{\psi} \right) \mpunkt \label{eq:139}
\end{align}
Hierbei gilt
\begin{subequations}
  \begin{align}
    \beta^\dagger &= \beta \\
    {\alpha^i}^\dagger &= \alpha^i \\
    \varrho &= \bm{\psi}^\dagger \bm{\psi} \\
    j^0 &= c \varrho \\
    j^k &= c \bm{\psi}^\dagger \alpha^k \bm{\psi}
    \intertext{und es folgt}
    \partial_\mu j^\mu &= 0.
  \end{align}
\end{subequations}
Dies entspricht einer \acct{Kontinuitätsgleichung} für die positiv
definite Dichte $\varrho$!

\subsection{Ruhende Teilchen}

Die Dirac-Gleichung für das ruhende Teilchen, also
\begin{align}
  \bm{p} &= \hbar \bm{k} = 0
\end{align}
lautet
\begin{align}
  \nonumber
  \ii \hbar \partial_t \bm{\psi}(\bm{r},t) &= m c^2 \gamma^0 \bm{\psi}(\bm{r},t) \\
  &= mc^2
  \begin{pmatrix}
    \mathds{1} & 0            \\ 
    0          & - \mathds{1}
  \end{pmatrix}
  \begin{pmatrix}
    \psi_1 \\ \psi_2 \\ \psi_3 \\ \psi_4
  \end{pmatrix}.
\end{align}
Diese hat vier Lösungen
\begin{equation}
  \begin{split}
    \psi_1^{(+)} &= \ee^{- \ii \frac{m c^2}{\hbar} t}
    \begin{pmatrix}
      1 \\ 0 \\ 0 \\ 0
    \end{pmatrix} \;,\\
    \psi_1^{(-)} &= \ee^{ \ii \frac{m c^2}{\hbar} t}
    \begin{pmatrix}
      0 \\ 0 \\ 1 \\ 0
    \end{pmatrix} \;,
  \end{split}
  \qquad
  \begin{split}
    \psi_2^{(+)} &= \ee^{- \ii \frac{m c^2}{\hbar} t}
    \begin{pmatrix}
      0 \\ 1 \\ 0 \\ 0
    \end{pmatrix} \;,\\
    \psi_2^{(-)} &= \ee^{\ii \frac{m c^2}{\hbar} t}
    \begin{pmatrix}
      0 \\ 0 \\ 0 \\ 1
    \end{pmatrix} \;.
  \end{split}
\end{equation}
Wir erhalten also jeweils zwei entartete Zustände positiver und negativer
Energie. Auf die energetisch entarteten Komponenten $(+)$ oder
$(-)$ wirken in~\eqref{eq:2014-10-21-02} gerade die
Pauli-Spinmatrizen, dies entspricht den beiden Einstellungen des
Spin $1/2$.

\begin{notice}[Folge:]
  Die Dirac-Gleichung beschreibt offensichtlich Spin-$1/2$-Teilchen,
  kann also die gesuchte relativistische Gleichung für Elektronen
  sein.
\end{notice}

\begin{notice}[Aber:]
  Es gibt auch die Zustände negativer Energie!
\end{notice}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../QFT.tex"
%%% End: