% Marcel Klett
Tatsächlich findet man in der Quantenelelektrodynamik unendlich viele divergierende Terme, die sich aber auf endlich viele primitiv-divergente reduzieren lassen, aus denen alle weiteren zusammengesetzt werden. Durch einfaches Abzählen der Potenzen der Propagatoren sowie aus der Kenntnis der Zahl der Linien an den Vertizes lässt sich folgende Regel ableiten:
\begin{equation}
  \label{eq:523}
  D = 4 - 3 \frac{F_a}{2} - P_a \mpunkt
\end{equation}
Dabei ist $D$ der Divergenzgrad, der eine logarithmische Divergenz für den Fall $D=0$ beschreibt, wohingegen $D = n >0$ die Potenz der Divergenz angibt, $F_a$ ist die Anzahl der äußeren Fermionenlinien und $P_a$ die Anzahl der äußeren Photonenlinien. 

\subsection{Problemloses Beispiel: Photon-Photon-Streuung}
\begin{minipage}{0.4\linewidth}
  \begin{tikzpicture}
    \matrix[feyn={.7,.7}] (m) {
      |(a)| &               &               & |(b)| \\
            & |[vertex](c)| & |[vertex](d)| &       \\
            & |[vertex](e)| & |[vertex](f)| &       \\
      |(g)| &               &               & |(h)| \\
    };
    \draw[fermion] (c) to (e);
    \draw[fermion] (e) to (f);
    \draw[fermion] (f) to (d);
    \draw[fermion] (d) to (c);
    \draw[photon] (a) to (c);
    \draw[photon] (b) to (d);
    \draw[photon] (g) to (e);
    \draw[photon] (f) to (h);
  \end{tikzpicture}   
\end{minipage}%
\begin{minipage}{0.6\linewidth}
  \begin{itemize}
  \item zwei einlaufende Photonen
  \item zwei auslaufende Photonen
  \item Photonen streuen an Photonen
  \end{itemize}
\end{minipage}
Im Gegensatz zur klassischen Elektrodynamik (Superpositionsprinzip)
gibt es die Photon-Photon-Streuung in der
Quantenelektrodynamik. Nach~\eqref{eq:523} ergibt sich für die
Photon-Photon Streuung einen Divergenzgrad von $D=0$, was eine
logarithmische Divergenz bedeutet. Tatsächlich sind es jedoch innere
Symmetrien (hier die Eichinvarianz der Elektrodynamik), die dazu
führen, dass sich divergierende Teilintegrale gegenseitig
aufheben. Damit konvergiert die Photon-Photon Streuung. Experimentell
wurde diese Art der Lichtstreuung noch nicht nachgewiesen, da der
Effekt stark unterdrückt ist ($4$ innere Fermionlinien werden
benötigt).

\subsection{Verbleibende divergente Terme}
Neben den Vakuumblasen, die in den Übergangsamplituden nur einen
irrelevanten Phasenfaktor liefern, müssen drei primitiv-divergente
Graphen betrachtet werden, deren Divergenz nicht in irgendeiner Form
kompensiert werden kann.
\begin{itemize}
\item Selbstenergie des Elektrons/Positrons $D=1$
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn={2,2}] (m) {
      |(a)| \\
      |(b)| \\
    };
    \draw[fermion] (b) to coordinate[pos=.2] (sb) coordinate[pos=.8] (sa) (a);
    \draw[backup photon, floop] (sa) to (sb);
  \end{tikzpicture}
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn={2,2}] (m) {
      |(a)| \\
      |(b)| \\
    };
    \draw[fermion] (a) to coordinate[pos=.2] (sa) coordinate[pos=.8] (sb) (b);
    \draw[backup photon, floop] (sa) to (sb);
  \end{tikzpicture}
\item Selbstenergie Photons $D=2$  
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn] (m) {
      |(d)|         \\
      |[vertex](a)| \\
      |[vertex](c)| \\
      |(b)|         \\
    };
    \draw[fermion, floop] (a) to (c);
    \draw[fermion, floop] (c) to (a);
    \draw[photon] (d) to (a);
    \draw[photon] (b) to (c);
  \end{tikzpicture} 
\item Vertexkorrektur $D=0$  
  \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
    \matrix[feyn] (m) {
      |(e)| &               &               &               & |(f)| \\
            & |[vertex](a)| &               & |[vertex](b)| &       \\
            &               & |[vertex](c)| &               &       \\
            &               & |(d)|         &               &       \\
    };
    \draw[fermion] (c) to (a);
    \draw[fermion] (b) to (c);
    \draw[photon] (d) to (c);
    \draw[photon] (b) to (a);
    \draw (b) to (f);
    \draw (a) to (e);
  \end{tikzpicture} 
\end{itemize}
Alle divergierenden Diagramme höherer Ordnung enthalten diese Graphen.

\section{Ideenskizze zur Regularisierung und Renormierung}

Die Divergenzen können ein grundlegendes Scheitern der Theorie bedeuten. Es gibt jedoch eine Möglichkeit so so zu behandeln, dass sich ein konsistentes Ergebnis ableiten lässt, das in hervorragender Übereinstimmung mit dem Experiment ist.
Die Grundlegende Idee besteht darin, dass alle Divergenzen in prinzipiell nicht messbare Konstanten \emph{verschoben} werden. Dies hat zur Folge, dass die Wirkungsquerschnitte theoretisch berechenbar bleiben.

\subsection{Ansätze zur Regularisierung}
Die Regularisierung enthält den Schritt, die Divergenzen von regulären
Anteilen der Integrale in additive Terme abzuspalten. Dabei gibt es
zwei Ansätze:
\begin{itemize}
\item \acct{Pauli-Villars-Regularisierung}: Terme mit derselben
  divergierenden Asymptotik werden abgezogen. Betrachten wir zum
  Beispiel die Selbstenergie des Elektrons
  \begin{equation*}
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn={1,1}] (m) {
        |(a)| \\
        |(b)| \\
      };
      \draw[fermion] (b) to (a);
    \end{tikzpicture} +
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn={1,1}] (m) {
        |(a)| \\
        |(b)| \\
      };
      \draw[fermion] (b) to coordinate[pos=.2] (sb) coordinate[pos=.8] (sa) (a);
      \draw[backup photon, floop] (sa) to (sb);
    \end{tikzpicture}     
  \end{equation*}
  \begin{subequations}
    \begin{equation}
      \label{eq:524a}
      \tilde{S}_F(q) + \tilde{S}_F(q) \Sigma^{(2)}(q) \tilde{S}_F(q) = \tilde{S}'^{(2)}_F(q) 
    \end{equation}
    mit 
    \begin{equation}
      \label{eq:524b}
      \Sigma^{(2)}(q) = \ii \frac{e^2}{\hbar} \int \frac{\diff^4 k}{(2\pi)^4} \tilde{D}_{F_{\alpha \beta}}(k) \gamma^\alpha \tilde{S}_F(q-k) \gamma^\beta \mcomma
    \end{equation}
  \end{subequations}
  wobei für den Phtonenpropagator $ \tilde{D}_{F_{\alpha \beta}}(k)
  \propto \frac{1}{k_\mu k^\mu + \ii \varepsilon}$ gilt. Dies wird nun
  ersetzt durch
  \begin{equation*}
     \frac{1}{k_\mu k^\mu + \ii \varepsilon} \to     \frac{1}{k_\mu k^\mu + \ii \varepsilon} -     \frac{1}{k_\mu k^\mu + K_\mu K^\mu +\ii \varepsilon}
  \end{equation*}
  mit großen aber endlichen $K$. Es gilt dann für kleine $k \to 0$,
  dass der subtrahierte Term unbedeutend wird und für große $k \to
  \infty$ geht die Differenz gegen $0$, womit die Divergenz aufgehoben
  ist.
\item \acct{Dimensionsmäßige Regularisierung}: Der Ursprung der
  Divergenz ist die Potenz in $\diff^4 k$. Als Ansatz wird nun das
  Integral in der Dimension
  \begin{equation}
    \label{eq:525}
    d = 4 + \delta
  \end{equation}
  ausgewertet. Der Term in~\eqref{eq:524b} lautet explizit
  \begin{subequations}
    \begin{equation}
      \label{eq:526a}
      \Sigma^{(2)}(q) = \frac{-\ii e^2 \mu_0 c \eta_{\alpha \beta}}{(2\pi)^4} \int \diff^4 k \frac{\gamma^\alpha\left( \slashed{q} - \slashed{k} + \frac{mc}{\hbar} \right) \gamma^\beta}{(k_\mu k^\mu + \ii \varepsilon) (q-k)_\mu (q-k)^\mu - \left(\frac{mc}{\hbar} \right)^2 + \ii \varepsilon}
    \end{equation}
    oder in $d$ Dimensionen
    \begin{equation}
      \label{eq:526b}
      \Sigma^{(2)}(q) =  \frac{-\ii e^2 \mu_0 c l^{4-d}}{(2\pi)^d} \int \diff^d k \frac{\gamma^\alpha\left( \slashed{q} - \slashed{k} + \frac{mc}{\hbar} \right) \gamma_\alpha}{(k_\mu k^\mu + \ii \varepsilon) (q-k)_\mu (q-k)^\mu - \left(\frac{mc}{\hbar} \right)^2 + \ii \varepsilon} \mpunkt
    \end{equation}
  \end{subequations}
  Der Faktor $l^{4-d}$ spiegelt hierbei eine Korrektur der Einheiten wider. Mit der Umrechnung
  \begin{subequations}
    \begin{equation}
      \label{eq:526a}
      \Sigma^{(2)}(q) = \frac{-\ii e^2 \mu_0 c \eta_{\alpha \beta}}{(2\pi)^4} \int \diff^4 k \frac{\gamma^\alpha\left( \slashed{q} - \slashed{k} + \frac{mc}{\hbar} \right) \gamma^\beta}{(k_\mu k^\mu + \ii \varepsilon) \left[ (q-k)_\mu (q-k)^\mu - \left(\frac{mc}{\hbar} \right)^2 + \ii \varepsilon\right]}
    \end{equation}
    oder in $d$ Dimensionen
    \begin{equation}
      \label{eq:526b}
      \Sigma^{(2)}(q) =  \frac{-\ii e^2 \mu_0 c l^{4-d}}{(2\pi)^d} \int \diff^d k \frac{\gamma^\alpha\left( \slashed{q} - \slashed{k} + \frac{mc}{\hbar} \right) \gamma_\alpha}{(k_\mu k^\mu + \ii \varepsilon) \left[(q-k)_\mu (q-k)^\mu - \left(\frac{mc}{\hbar} \right)^2 + \ii \varepsilon\right]} \mpunkt
    \end{equation}
  \end{subequations}
  Der Faktor $l^{4-d}$ spiegelt hierbei eine Korrektur der Einheiten wider. Mit der Umrechnung
\begin{equation}
  \label{eq:527}
  \frac{1}{ab} = \int_{0}^{1} \frac{\diff z}{(az + b(z+1))^2}
\end{equation}
wird daraus
\begin{equation}
  \label{eq:528}
  \Sigma^{(2)}(q) =  \frac{-\ii e^2 \mu_0 c l^{4-d}}{(2\pi)^d} \int_{0}^{1} \diff z \int  \frac{\gamma^\alpha\left( \slashed{q} - \slashed{k} + \frac{mc}{\hbar} \right) \gamma_\alpha \diff^d k}{ \left[(q-k)_\mu (q-k)^\mu z - \left(\frac{mc}{\hbar} \right)^2 z + k_\mu k^\mu (1+z) + \ii \varepsilon \right]^2} \mpunkt
\end{equation}
In einigen Rechenschritten kann man zeigen, dass
\begin{subequations}
  \begin{equation}
    \label{eq:528}
    \Sigma^{(2)}(q) =  \frac{-\ii e^2 \mu_0 c l^{4-d}}{(2\pi)^d} \int_{0}^{1} \diff z \int \diff^d k \frac{\gamma^\alpha\left( \slashed{q} - \slashed{k} + \frac{mc}{\hbar} \right) \gamma_\alpha}{ \left[(q-k)_\mu (q-k)^\mu z - \left(\frac{mc}{\hbar} \right)^2 z + k_\mu k^\mu (1+z) + \ii \varepsilon \right]^2} \mpunkt
  \end{equation}
  in einen divergierenden Anteil $\propto \delta^{-1}$ (für $d=4$ gilt $\delta \to 0$) und einen regulären
  \begin{align}
    \notag
    R_{\Sigma}(q) &= \frac{(1+\xi) \slashed{q}}{2} - (1+2\xi) \frac{m c}{\hbar}   \\
    &+ \int_{0}^{1} \left[ (1-z)\slashed{q} - 2 \frac{mc}{\hbar} \right] \ln \left( \frac{m^2 c^2 z - q_\mu q^\mu z(1-z)\hbar^2}{4 \pi \mu_0^2 \hbar^2} \right) \diff z
    \label{eq:529b}  
  \end{align}
  zerfällt. Dabei ist $\xi$ eine Integrationskonstante.
\end{subequations}
\end{itemize}

\subsection{Konvergente Matrixelemente durch Renormierung}
Der erste Schritt in der Beseitigung der Divergenzen besteht darin,~\eqref{eq:524a} umzuschreiben
\begin{equation}
  \label{eq:530}
  \tilde{S}'^{(2)}_F = \tilde{S}_F(q) + \tilde{S}_F(q) \Sigma^{(2)}(q) \tilde{S}_F(q) \approx \frac{1}{\tilde{S}^{-1}_F(q) - \Sigma^{(2)}(q)} \mpunkt
\end{equation}
Tatsächlich ist dies bis auf Terme der Ordnung
$\mathcal{O}\bigl[\bigl(\Sigma^{(2)}(q)\bigr)^2\bigr]$ identisch
mit~\eqref{eq:524a}
\begin{equation*}
  \begin{array}{@{}c@{}c@{{}+{}}c@{{}+{}}c@{{}+{}}c@{}}
    \dfrac{1}{\tilde{S}^{-1}_F(q) - \Sigma^{(2)}(q)} = {}
    &\tilde{S}_F(q)
    & \tilde{S}_F(q) \Sigma^{(2)}(q) \tilde{S}_F(q)
    & \tilde{S}_F(q) \Sigma^{(2)}(q) \tilde{S}_F(q) \Sigma^{(2)}(q) \tilde{S}_F(q)
    & \ldots \\
    &
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn={2,1}] (m) {
        |(a)| \\
        |(b)| \\
      };
      \draw[fermion] (b) to (a);
    \end{tikzpicture}
    &
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn={2,1}] (m) {
        |(a)| \\
        |(b)| \\
      };
      \draw[fermion] (b) to node[vertex,pos=.2] (sb) {} node[vertex,pos=.8] (sa) {} (a);
      \draw[backup photon, floop] (sa) to (sb);
    \end{tikzpicture}
    &
    \begin{tikzpicture}[baseline={([yshift=\dimexpr-\fontdimen22\textfont2\relax]m.center)}]
      \matrix[feyn={2,1}] (m) {
        |(a)| \\
        |(b)| \\
      };
      \draw[fermion] (b) to
      node[vertex,pos=.1] (s1) {}
      node[vertex,pos=.4] (s2) {}
      node[vertex,pos=.6] (s3) {}
      node[vertex,pos=.9] (s4) {} (a);
      \draw[backup photon, floop] (s2) to (s1);
      \draw[backup photon, floop] (s4) to (s3);
    \end{tikzpicture}
    & \ldots
  \end{array}
\end{equation*}
und enthält in konsequenter Reihenfolge alle höheren Beiträge von Korrekturen desselben Typs.
\begin{align}
  \notag 
  \tilde{S}'^{(2)}_F(q) &\eqrel{eq:530}[eq:529a]{=} \frac{1}{\slashed{q} - \frac{mc}{\hbar} - \frac{\alpha}{2\pi} \left( \frac{4 \frac{mc}{\hbar} - \slashed{q}}{\delta} + R_\Sigma(q)\right)} \\
  &\approx \frac{1 - \frac{\alpha}{2\pi \delta}}{\slashed{q} - \frac{mc}{\hbar} \left( 1 + \frac{3\alpha}{2\pi \delta} + \frac{\alpha \hbar}{2\pi} + \frac{R_\Sigma(q)}{mc} \right)} + \mathcal{O}\left(\alpha^2 \right) \label{eq:531}
\end{align}
Jetzt führen wir ein
\begin{equation}
  \label{eq:532}
  m = m_r + \underbrace{\Delta_m}_{\mathcal{O} (\alpha)} + \mathcal{O} \left( \alpha \right)^2 \mcomma
\end{equation}
dann wird der Nenner in Gleichung~\eqref{eq:531} zu
\begin{equation}
  \label{eq:533}
  \slashed{q} - \frac{m_r c}{\hbar} - \frac{\Delta_m c}{\hbar} - \frac{3 \alpha m_r c}{2 \pi \hbar \delta} + \mathcal{O} \left( \alpha \right)^2 \mpunkt
\end{equation}
Dieser Term divergiert genau dann nicht mehr, sofern 
\begin{equation}
  \label{eq:534}
  \Delta m = - \frac{3 \alpha m_r}{2 \pi \delta}
\end{equation}
gilt. 

Auf ähnliche Weise absorbiert man die Divergenz im Zähler. Zu beachten
gilt, dass die inneren Linien immer zusammen mit Vertizes und weiteren
dort verknüpften Linien auftreten. Dabei ist der Vertex proportional
zur Elementarladung $e$, welche wiederum aufgespalten werden kann in
\begin{subequations}
  \begin{equation}
    \label{eq:535a}
    e = e_r + \Delta e \mpunkt
  \end{equation}  
  Der Zähler aus~\eqref{eq:531} divergiert genau dann nicht, wenn
  \begin{equation}
    \label{eq:535b}
    \Delta e = \frac{\alpha e_r}{3 \pi \delta} \mpunkt
  \end{equation}
\end{subequations}
Diese Renormierung gelingt konsistent in der Quantenelektrodynamik,
das heißt in allen divergierenden Termen mit denselben Werten. Es
treten in allen messbaren Größen nur die renormierten Massen $m_r$ und
Ladungen $e_r$ auf. Die nackten Massen $m$ und Ladungen $e$, die wie
$\Delta m$ und $\Delta e$ divergieren, sind nun reine Rechengrößen
ohne physikalische Relevanz. Ihr Divergieren spielt keine Rolle für
die Theorie.
