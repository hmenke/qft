% Michael Schmid

\chapter{Quantisierung freier relativistischer Felder}

\section{Klein-Gordon-Feld}

\subsection{Motivation der Herangehensweise}

Im vorherigen Abschnitt wurde eine \enquote{Wunschliste} an die
Theorie für die Elementarteilchen erstellt. Darin forderten wir eine
einheitliche Beschreibung mit der Elektrodynamik. Eine Quantisierung
der elektrischen und magnetischen Felder (Photonen) gelingt, indem man
die Feldvektoren nach Gleichung~\eqref{eq:229a} und~\eqref{eq:229b}
durch Opertoren ersetzen.

\begin{notice}[Ansatz]
  Ersetze auch das Feld $\psi$ der Klein-Gordon-Gleichung durch
  Operatoren, dies führt auf eine Quantisierung.
\end{notice}

\begin{notice}[Allerdings]
  $\psi$ enstammt bereits einer Gleichung einer Quantentheorie,
  dies führt auf den Begriff der \acct{zweiten Quantisierung}.
\end{notice}

\begin{notice}[Frage]
  Wird hier tatsächlich zum zweiten Mal quantisiert?
\end{notice}

Nein, die Quantisierung in der Klein-Gordon-Gleichung oder auch der
Schrödingergleichung erfolgt erst durch die Suche nach konkreten
Lösungen für ein gegebenes Problem (Potential), die die geforderte
Nebenbedingungen (Normierbarkeit!) erfüllen. Wir gehen genau von
diesen Lösungen aus, stellen keine neuen Bedingungen, sondern erlauben
nur eine mehrfache Besetzung dieser Lösungen und erhalten eine
Vielteilchengleichung. Dies stellt einen anderen Ansatz dar, die
Quantisierung zu verstehen, jedoch keine tatsächliche zweite
Quantisierung eines quantenmechanischen Ergebnisses $\psi$.

\subsection{Kanonische Variablen}

Für die Quantisierung nach Gleichung~\eqref{eq:229a} werden kanonische
Variablen benötigt. Dafür führen wir die Lagrangedichte
\begin{align}
  \nonumber
  \mathcal{L} &= \frac{\hbar^2}{2m} \eta^{\mu \nu} (\partial_\mu \phi) (\partial_\nu \phi^\ast) - \frac{m c^2}{2} \phi \phi^\ast \\
  \label{eq:31}
  &= \frac{\hbar^2}{2m} \phi_{,\mu} \phi^\ast{}^{,\mu} - \frac{m c^2}{2} \phi \phi^\ast
\end{align}
für das komplexe Feld $\phi$ ein. Es beschreibt
dabei zwei unabhängige, reelle Felder,
\begin{align}
  \phi = \phi_1 + \ii \phi_2 \mcomma \nonumber
\end{align}
d.h.\ entsprechend können $\phi$ und $\phi^\ast$ als unabhängige
Felder aufgefasst werden. Die zugehörigen Euler-Lagrange-Gleichungen lauten
\begin{align}
  0 &\eqrel{eq:216}{=} \frac{\partial \mathcal{L}}{\partial \phi^\ast} - \frac{\partial}{\partial x^\alpha} \frac{\partial \mathcal{L}}{\partial \phi^\ast_{,\alpha}} \nonumber \\
  &\eqrel{eq:31}{=} - \frac{mc^2}{2} \phi -  \frac{\hbar^2}{2m} \partial_\alpha \eta^{\mu \alpha} \partial_\mu \phi \nonumber \\
  &= - \frac{mc^2}{2} \phi - \frac{\hbar^2}{2m} \partial_\mu \partial^\mu \phi \nonumber 
\end{align}
oder
\begin{align}
  \left[ \partial_\mu \partial^\mu + \left( \frac{m c}{\hbar} \right)^2  \right] \phi = 0. \label{eq:32}
\end{align}
Weiterhin:
\begin{align}
  0 &= \frac{\partial \mathcal{L}}{\partial \phi} - \partial_\mu \frac{\partial \mathcal{L}}{\partial \phi_{,\mu}} \nonumber \\
  &= - \frac{m c^2}{2} \phi^\ast - \frac{\hbar^2}{2m} \partial_\mu\partial^\mu \phi^\ast \mcomma \nonumber \\
  0 &= \left[ \partial_\mu \partial^\mu + \left( \frac{m c}{\hbar} \right)^2  \right] \phi^\ast \mpunkt \label{eq:33}
\end{align}
Wir erhalten die Klein-Gordon-Gleichung und ihr komplex
konjugiertes. Damit ist $\mathcal{L}$ also geeignet, um das
\acct{Klein-Gordon-Feld} zu beschreiben.

Führen wir nun die Impulsdichten über
\begin{subequations}
  \begin{align}
    \mathcal{L} &\eqrel{eq:31}{=} \frac{\hbar^2}{2m c^2} \dot{\phi} \dot{\phi}^\ast - \frac{\hbar^2}{2m} (\nabla \phi) (\nabla \phi^\ast) - \frac{mc^2}{2}\phi \phi^\ast, \label{eq:35a} \\
    \pi &\eqrel{eq:217}{=} \frac{\partial \mathcal{L}}{\partial \dot{\phi}} = \frac{\hbar^2}{2m c^2} \dot{\phi}^\ast \label{eq:35b}, \\
    \pi^\ast &\eqrel{eq:217}{=} \frac{\partial \mathcal{L}}{\partial \dot{\phi}^\ast} = \frac{\hbar^2}{2 m c^2} \dot{\phi} \label{eq:35c} 
  \end{align}
\end{subequations}
ein. Eine Umkehrung ergibt
\begin{subequations}
  \begin{align}
    \dot{\phi}^\ast &\eqrel{eq:35a}{=} \frac{2mc^2}{\hbar^2} \pi \mcomma \label{eq:36b} \\
    \dot{\phi} &\eqrel{eq:35b}{=} \frac{2mc^2}{\hbar^2} \pi^\ast \mpunkt \label{eq:36c} 
  \end{align}
\end{subequations}
Damit wird die Hamiltondichte zu:
\begin{align}
  \mathcal{H} &\eqrel{eq:219}{=} \pi \dot{\phi} + \pi^\ast \dot{\phi}^\ast - \mathcal{L} \nonumber \\
  &= \frac{2 mc^2}{\hbar^2} \pi \pi^\ast + \frac{2mc^2}{\hbar^2} \pi^\ast \pi - \frac{2mc^2}{\hbar^2} \pi^\ast \pi + \frac{\hbar^2}{2m} (\nabla \phi)(\nabla \phi^\ast) + \frac{m c^2}{2} \phi \phi^\ast \nonumber \\
  &= \frac{2m c^2}{2} \pi \pi^\ast + \frac{\hbar^2}{2m} (\nabla \phi)(\nabla \phi^\ast) + \frac{mc^2}{2} \phi \phi^\ast. \label{eq:37}
\end{align}

\subsection{Quantisierung der Klein-Gordon-Gleichung}
\label{sec:313}

Eine Quantisierung erfolgt durch den Übergang zu Operatoren
\begin{align}
  \pi, \pi^\ast, \phi, \phi^\ast \to  \hat{\pi}, \hat{\pi}^\dagger, \hat{\phi}, \hat{\phi}^\dagger \mcomma \label{eq:38}
\end{align}
die die Vertauschungsrelationen~\eqref{eq:229a} und~\eqref{eq:229b}
erfüllen, also explizit
\begin{subequations}
  \begin{align}
    [\phi,\phi] &= [\phi,\phi^\dagger]  = [\phi^\dagger,\phi^\dagger] = [\pi,\pi]= [\pi,\pi^\dagger] = [\pi^\dagger, \pi^\dagger] = 0 \mcomma \label{eq:39a} \\
    [\phi,\pi^\dagger] &= [\phi^\dagger,\pi] =0 \mcomma \label{eq:39b} \\
    [\phi(\bm{r}), \pi(\bm{r}')] &= [\phi^\dagger(\bm{r}),\pi^\dagger(\bm{r}')] = \ii \hbar \delta(\bm{r}-\bm{r}') \mpunkt \label{eq:39c}
  \end{align}
\end{subequations}
Die Ersetzung~\eqref{eq:38} zusammen mit~\eqref{eq:36a}
und~\eqref{eq:36b} ergibt, dass auch die Feldoperatoren $\hat{\phi}$, $\hat{\pi} \propto \dot{\hat{\phi}}^\dagger$ die
Klein-Gordon-Gleichung~\eqref{eq:32} und~\eqref{eq:33} erfüllen
müssen.

Ein geeignetes Vorgehen besteht darin, die Feldoperatoren nach den
Lösungen der freien Klein-Gordon-Gleichung zu entwickeln.
\begin{subequations}
  \begin{align}
    \phi_{\bm{k}} &\propto \ee^{\mp\ii(E t - \bm{p} \cdot \bm{r})/\hbar} \label{eq:310a}
    \intertext{Verwende die Basisfunktionen}
    f_{\bm{k}} &= \frac{1}{(2 \pi )^{3/2}} \sqrt{\frac{\hbar}{2E(\bm{k})}} \ee^{-\ii k_\mu x^\mu} \label{eq:310b}
  \end{align}
\end{subequations}
mit den Orthogonalitätsrelationen
\begin{subequations}
  \begin{align}
    \int f_{\bm{k}}^\ast(\bm{r},t) \ii \rlvec{\partial}_t f_{\bm{k}'}'(\bm{r},t)   \diff^3\bm{r} &= \delta(\bm{k}-\bm{k}') \label{eq:311a} \\
    \int f_{\bm{k}}^{(\ast)}(\bm{r},t) \ii \rlvec{\partial}_t f_{\bm{k}'}^{(\ast)}(\bm{r},t) \diff^3\bm{r} &= 0 \label{eq:311b}
    \intertext{mit}
    a \rlvec{\partial}_t b = a \partial_t b - (\partial_t a)b. \label{eq:311c}
  \end{align}
\end{subequations}
Damit 
\begin{subequations}
  \label{eq:312}
  \begin{align}
    \hat{\phi}(\bm{r},t) &= \frac{2mc^2}{\hbar} \int \left(  \hat{a}_{\bm{k}} f_{\bm{k}}(\bm{r},t) + \hat{c}_{\bm{k}}^\dagger f_{\bm{k}}^\ast(\bm{r},t)  \right) \diff^3\bm{k} \mcomma \label{eq:312a} \\
    \hat{\phi}^\dagger(\bm{r},t) &= \frac{2mc^2}{\hbar} \int \left(  \hat{a}_{\bm{k}}^\dagger f_{\bm{k}}^\ast(\bm{r},t) + \hat{c}_{\bm{k}} f_{\bm{k}}(\bm{r},t)  \right) \diff^3\bm{k} \mcomma \label{eq:312b}
  \end{align}
\end{subequations}
wobei die Operatoreigenschaft nun von den Entwicklungskoeffizienten
$\hat{a}_{\bm{k}}$ und $\hat{c}_{\bm{k}}$ getragen werden muss. Unter
Verwendung von Gleichung~\eqref{eq:311a} bis~\eqref{eq:311c} erhält
man die Umkehrung
\begin{subequations}
  \begin{align}
    \hat{a}_{\bm{k}} &= \sqrt{\frac{\hbar^2}{2m c^2}} \int f_{\bm{k}}^\ast (\bm{r},t) \ii \rlvec{\partial}_t \hat{\phi}(\bm{r},t) \diff^3 \bm{r} \mcomma \label{eq:313a} \\
    \hat{c}_{\bm{k}} &= \sqrt{\frac{\hbar^2}{2m c^2}} \int f_{\bm{k}}^\ast (\bm{r},t) \ii \rlvec{\partial}_t \hat{\phi}^\dagger(\bm{r},t) \diff^3 \bm{r} \mcomma \label{eq:313b} \\
    \hat{a}_{\bm{k}}^\dagger &= -\sqrt{\frac{\hbar^2}{2m c^2}} \int f_{\bm{k}}(\bm{r},t) \ii \rlvec{\partial}_t \hat{\phi}^\dagger(\bm{r},t) \diff^3 \bm{r} \mcomma \label{eq:313c} \\
    \hat{c}_{\bm{k}}^\dagger &= -\sqrt{\frac{\hbar^2}{2m c^2}} \int f_{\bm{k}} (\bm{r},t) \ii \rlvec{\partial}_t \hat{\phi}(\bm{r},t) \diff^3 \bm{r} \mpunkt \label{eq:313d} 
  \end{align}
\end{subequations}

Damit lassen sich die Kommutatorregeln für die Operatoren $\hat{a}$
und $\hat{c}$ finden,
\begin{multline}
  [\hat{a}_{\bm{k}},\hat{a}_{\bm{k}}^\dagger] \eqrel{eq:313a}[eq:313c]{=} \frac{\hbar^2}{2 m c^2 } \int \diff^3 \bm{r} \int \diff^3 \bm{r}' \bigg\{  f_{\bm{k}}^\ast (\bm{r},t) f_{\bm{k}'}(\bm{r}',t) \underbrace{\left[ \partial_t \hat{\phi}(\bm{r},t), \partial_t \hat{\phi}^\dagger (\bm{r}',t)  \right]}_{ \propto [\pi^\dagger(\bm{r},t) , \pi(\bm{r}',t)] = 0}  \\
  - \left( \partial_t f_{\bm{k}}^\ast (\bm{r},t) \right) f_{\bm{k}'}(\bm{r}',t) \underbrace{\left[ \hat{\phi}(\bm{r},t), \partial_t \hat{\phi}^\dagger (\bm{r}',t) \right]}_{= \frac{2 \ii m c^2}{\hbar^2}\delta(\bm{r}-\bm{r}')} \\
  -  f_{\bm{k}}^\ast (\bm{r},t) \left( \partial_t f_{\bm{k}'}(\bm{r}',t)  \right) \underbrace{ \left[ \partial_t \hat{\phi}(\bm{r},t), \hat{\phi}^\dagger(\bm{r}',t) \right]  }_{=-\frac{2 \ii m c^2}{\hbar} \delta(\bm{r}- \bm{r}')} \\ 
  + \left( \partial_t f_{\bm{k}}^\ast (\bm{r},t)\right) \left( \partial_t f_{\bm{k}'}(\bm{r}',t)  \right) \underbrace{ \left[ \hat{\phi}(\bm{r},t), \hat{\phi}^\dagger(\bm{r}',t) \right]  }_{=0}   \bigg\} \mpunkt \nonumber
\end{multline}
Insgesamt ergibt sich
\begin{subequations}
  \label{eq:314}
  \begin{align}
    [\hat{a}_{\bm{k}},\hat{a}_{\bm{k}}^\dagger] &= \int \diff^3 \bm{r} f_{\bm{k}}^\ast(\bm{r},t) \ii \rlvec{\partial}_t f_{\bm{k}}(\bm{r},t) \nonumber \\
    &\eqrel{eq:311a}{=} \delta(\bm{k} - \bm{k}') \mpunkt  \label{eq:314a} 
    \intertext{Analog findet man}
    \left[\hat{a}_{\bm{k}}, \hat{a}_{\bm{k}'} \right] &=  \left[\hat{a}_{\bm{k}}^\dagger, \hat{a}_{\bm{k}'}^\dagger \right] = 0 \mcomma \label{eq:314b} \\
    \left[\hat{c}_{\bm{k}}, \hat{c}_{\bm{k}'}^\dagger \right] &= \delta(\bm{k} - \bm{k}') \mcomma \label{eq:314c} \\
     \left[\hat{c}_{\bm{k}}, \hat{c}_{\bm{k}'} \right] &=  \left[\hat{c}_{\bm{k}}^\dagger, \hat{c}^\dagger_{\bm{k}'} \right] = 0 \mpunkt\label{eq:314d}
  \end{align}
\end{subequations}
Die Operatoren $\hat{a}_{\bm{k}}$ und $\hat{c}_{\bm{k}}$ sind also
Vernichter der Moden $\bm{k}$ mit $+$ oder $-$ im Exponenten. Demnach
sind $\hat{a}_{\bm{k}}^\dagger$ und $\hat{c}_{\bm{k}}^\dagger$ die
zugehörigen Erzeuger.

\subsection{Eigenzustände des Hamiltonoperators und deren Interpretation}

\paragraph{a) Hamiltonoperator:}
Mit den Feldoperatoren~\eqref{eq:312a} und~\eqref{eq:312b} und den
zugehörigen Impusdichten
\begin{subequations}
  \label{eq:315}
  \begin{align}
    \hat{\pi}(\bm{r},t) &\eqrel{eq:35b}{=} \frac{\hbar^2}{2mc^2} \partial_t \hat{\phi}^\dagger \mcomma \nonumber \\
    &= \ii \sqrt{\frac{\hbar}{2mc^2}} \int \diff^3\bm{k} E(\bm{k}) \left\{ \hat{a}_\bm{k}^\dagger f_{\bm{k}}^\ast (\bm{r},t)  - \hat{c}_{\bm{k}} f_{\bm{k}}(\bm{r},t) \right\} \label{eq:315a} \\ 
      \hat{\pi}^\dagger(\bm{r},t) &=- \ii \sqrt{\frac{\hbar}{2mc^2}} \int \diff^3\bm{k} E(\bm{k}) \left\{ \hat{a}_\bm{k} f_{\bm{k}} (\bm{r},t)  - \hat{c}_{\bm{k}}^\dagger f_{\bm{k}}^\ast(\bm{r},t) \right\} \label{eq:315b} 
  \end{align}
\end{subequations}
und
\begin{align}
  E(\bm{k}) &= + \sqrt{\hbar^2 \bm{k}^2 c^2 + m^2 c^4} \label{eq:315c}
\end{align}
lässt sich die Hamiltondichte explizit in der Modenentwicklung angeben.
\begin{align}
  \mathcal{H} &\eqrel{eq:37}{=} \frac{2mc^2}{\hbar^2} \hat{\pi}\hat{\pi}^\dagger + \frac{\hbar^2}{2m} (\nabla \hat{\phi})(\nabla \hat{\phi}^\dagger) + \frac{m c^2}{2} \hat{\phi}\hat{\phi}^\dagger.
\end{align}
Mit Gleichung~\eqref{eq:310a}, \eqref{eq:315} und~\eqref{eq:312} folgt
\begin{multline}
  \mathcal{H} = \frac{1}{\hbar}\int \diff^3 \bm{k} \int \diff^3 \bm{k}' \bigg\{ \hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}'} f_{\bm{k}}^\ast (\bm{r},t) f_{\bm{k}'}(\bm{r},t) - \hat{a}_{\bm{k}}^\dagger \hat{c}_{\bm{k}'}^\dagger  f_{\bm{k}}^\ast(\bm{r},t) f_{\bm{k}'}^\ast (\bm{r},t) \\
  - \hat{c}_{\bm{k}} \hat{a}_{\bm{k}'} f_{\bm{k}}(\bm{r},t) f_{\bm{k}'}(\bm{r},t) + \hat{c}_{\bm{k}} \hat{c}_{\bm{k}'}^\dagger f_{\bm{k}}(\bm{r},t) f_{\bm{k}'}^\ast (\bm{r},t) \bigg\} E(\bm{k}) E(\bm{k}')\\
+ \hbar c^2 \int \diff^3 \bm{k} \int \diff^3\bm{k}' (\bm{k}\cdot\bm{k}') \bigg\{ \hat{a}_{\bm{k}} \hat{a}_{\bm{k}'}^\dagger  f_{\bm{k}} (\bm{r},t) f^\ast_{\bm{k}'}(\bm{r},t) + \hat{a}_{\bm{k}} \hat{c}_{\bm{k}'}  f_{\bm{k}}(\bm{r},t) f_{\bm{k}'} (\bm{r},t) \\
  + \hat{c}_{\bm{k}}^\dagger \hat{a}^\dagger_{\bm{k}'} f_{\bm{k}}^\ast(\bm{r},t) f^\ast_{\bm{k}'}(\bm{r},t) + \hat{c}_{\bm{k}}^\dagger \hat{c}_{\bm{k}'} f_{\bm{k}}^\ast(\bm{r},t) f_{\bm{k}'} (\bm{r},t) \bigg\} \\
+ \frac{mc^2}{\hbar}  \int \diff^3 \bm{k} \int \diff^3\bm{k}' \bigg\{ \hat{a}_{\bm{k}} \hat{a}_{\bm{k}'}^\dagger  f_{\bm{k}} (\bm{r},t) f^\ast_{\bm{k}'}(\bm{r},t) + \hat{a}_{\bm{k}} \hat{c}_{\bm{k}'}  f_{\bm{k}}(\bm{r},t) f_{\bm{k}'} (\bm{r},t) \\
  + \hat{c}_{\bm{k}}^\dagger \hat{a}^\dagger_{\bm{k}'} f_{\bm{k}}^\ast(\bm{r},t) f^\ast_{\bm{k}'}(\bm{r},t) + \hat{c}_{\bm{k}}^\dagger \hat{c}_{\bm{k}'} f_{\bm{k}}^\ast(\bm{r},t) f_{\bm{k}'} (\bm{r},t) \bigg\} \mpunkt \label{eq:316}
\end{multline}
Mit
\begin{subequations}
  \begin{align}
    \int \diff^3 \bm{r} f_{\bm{k}}^\ast (\bm{r},t) f_{\bm{k}'}(\bm{r},t) &= \frac{\hbar}{2 E(\bm{k})} \delta(\bm{k} - \bm{k}') \mcomma \label{eq:317a}\\
    \int \diff^3 \bm{r} f_{\bm{k}}(\bm{r},t) f_{\bm{k}'}(\bm{r},t) &= \frac{\hbar}{2 E(\bm{k})} \ee^{-2\ii E(\bm{k})t/\hbar} \delta (\bm{k}+\bm{k}') \mcomma \label{eq:317b} \\
    \int \diff^3 \bm{r} f_{\bm{k}}^\ast (\bm{r},t) f_{\bm{k}'}(\bm{r},t) &= \frac{\hbar}{2 E(\bm{k})} \ee^{2\ii E(\bm{k}) t / \hbar} \delta(\bm{k} + \bm{k}') \label{eq:317c}
 \end{align}
 \label{eq:317}
\end{subequations}
ergibt sich für den Hamiltonoperator
\begin{multline}
  H = \int \diff^3 \bm{r} \mathcal{H} \eqrel{eq:316}[eq:317]{=} \frac{1}{2} \int \diff^3 \bm{k} E(\bm{k}) \left\{  \hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}} - \hat{a}_{\bm{k}}^\dagger \hat{c}_{-\bm{k}}^\dagger \ee^{2 \ii E t/\hbar} - \hat{c}_{\bm{k}} \hat{a}_{-\bm{k}}\ee^{2\ii Et/\hbar} + \hat{c}_{\bm{k}} \hat{c}_{\bm{k}}^\dagger \right\} \\
  + \frac{1}{2} \int \diff^3 \bm{k} \underbrace{(c^2 \hbar^2 \bm{k}^2 + m^2 c^4) \frac{1}{E(\bm{k})}}_{= E(\bm{k})} \left\{ \hat{a}_{\bm{k}}^\dagger + \hat{a}_{-\bm{k}} \hat{c}_{\bm{k}} \ee^{-2\ii E t/\hbar} + \hat{c}_{-\bm{k}}^\dagger \hat{a}_{\bm{k}}^\dagger \ee^{2 \ii E t/\hbar} + \hat{c}_{\bm{k}}^\dagger \hat{c}_{\bm{k}} \right\} \mpunkt \nonumber
\end{multline}
Damit erhalten wir
\begin{align}
  H &= \frac{1}{2} \int \diff^3 \bm{k} E(\bm{k}) \left\{ \hat{a}_{\bm{k}}^\dagger \hat{c}_{\bm{k}} + \hat{a}_{\bm{k}}\hat{a}_{\bm{k}}^\dagger + \hat{c}_{\bm{k}}^\dagger \hat{c}_{\bm{k}} + \hat{c}_{\bm{k}} \hat{c}_{\bm{k}}^\dagger  \right\} \nonumber  \\
&\eqrel{eq:314a}[eq:314b]{=} \int \diff^3 \bm{k} \left\{ \hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}} + \hat{c}_{\bm{k}}^\dagger \hat{c}_{\bm{k}} + \underbrace{\delta(0)}_{=1}   \right\} E(\bm{k}) \label{eq:318}
\end{align}
oder 
\begin{align}
  H &= \int \diff^3 \bm{k} E(\bm{k}) \left\{ \hat{n}_{a,\bm{k}} + \hat{n}_{c,\bm{k}} +1   \right\} \label{eq:319}
\end{align}
mit den Anzahloperator der $a$-, bzw.\ $c$-Zustände
\begin{subequations}
  \begin{align}
    \hat{n}_{a,\bm{k}} &= \hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}} \mcomma \label{eq:320a}\\
    \hat{n}_{c,\bm{k}} &= \hat{c}_{\bm{k}}^\dagger \hat{c}_{\bm{k}} \mpunkt \label{eq:320b}
  \end{align}
\end{subequations}

\paragraph{b) Eigenzustände:} Eigenzustände sind nun Fockraumzustände der Form
\begin{align*}
  \ket{n_{a,\bm{k}} n_{c,\bm{k}}} \mcomma 
\end{align*}
wobei
\begin{subequations}
  \begin{align}
    \hat{n}_{a,\bm{k}}   \ket{n_{a,\bm{k}} n_{c,\bm{k}}} &= n_{a,\bm{k}} \ket{n_{a,\bm{k}} n_{c,\bm{k}}} \label{eq:321a} \mcomma \\
    \hat{n}_{c,\bm{k}}   \ket{n_{a,\bm{k}} n_{c,\bm{k}}} &= n_{c,\bm{k}} \ket{n_{a,\bm{k}} n_{c,\bm{k}}} \label{eq:321b} \mcomma 
  \end{align}
\end{subequations}
die die Besetzung der $a$- und $c$-Zustände angeben.

\paragraph{c) Vakuumzustand und Normalordnung:} Der Vakuumzustand hat die Energie
\begin{align}
  H \ket{00} &= \int \diff^3 \bm{k} E(\bm{k}) \to \infty. \label{eq:322}
\end{align}
Der Anteil $\int \diff^3\bm{k} \left( \ldots + E(\bm{k}) \right)$ im Hamiltonoperator
hat sich aufgrund der Reihenfolge
\begin{align*}
  \hat{\pi} \hat{\pi}^\dagger, (\nabla \hat{\phi})(\nabla \hat{\phi}^\dagger), \hat{\phi} \hat{\phi}^\dagger
\end{align*}
in Gleichung~\eqref{eq:316} ergeben. Diese ist jedoch willkürlich!
Auch möglich wäre
\begin{align}
  \phi \phi^\ast = \phi^\ast \phi \to \hat{\phi}^\dagger \hat{\phi} \mcomma \label{eq:322}
\end{align}
dies hätte die Vakuumenergie $0$ zur Folge.  Messbar sind nur die
Energiedifferenzen, daher hat jede Reihenfolge ihre Berechtigung. Ein
übliches Vorgehen besteht darin, die \acct{Normalordnung} einzuführen,
die so gewählt ist, dass die Vakuumenergie verschwindet.
\begin{subequations}
  \begin{align}
    \nOrd{\hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}} + \hat{a}_{\bm{k}} \hat{a}_{\bm{k}}^\dagger} &= 2 \hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}} \label{eq:324a} \\
    \nOrd{H} &= \int \diff^3 \bm{k} E(\bm{k}) \left\{ \hat{a}_{\bm{k}}^\dagger \hat{a}_{\bm{k}} + \hat{c}_{\bm{k}}^\dagger c_{\bm{k}}   \right\} \label{eq:324b}
  \end{align}
\end{subequations}
Die ist unser neuer Hamiltonoperator. In der Schreibweise wird
$\nOrd{\ldots}$ oft unterschlagen.

\paragraph{d) Mesonen:} Um zu erkennen, welche Teilchen durch die
gefundenen Lösungen beschrieben werden, helfen Symmetrien der Wirkung
mit der Lagrangedichte~\eqref{eq:35a}.

Die Lagrangedichte ist invariant unter der $U(1)$ Phasentransformation 
\begin{align}
  \phi \to \ee^{-\ii \theta} \phi \label{eq:325}
\end{align}
und damit auch die Wirkung. Es folgt mit der infinitesimalen Transformation 
\begin{align}
  \diff \phi &= \phi-\phi \eqrel{eq:325}{=} \left(\ee^{-\ii \theta} -1 \right) \phi \approx - \ii \diff \theta \phi \label{eq:326}
\end{align}
aus den Gleichungen~\eqref{eq:231a} bis~\eqref{eq:231c} die Erhaltungsgröße
\begin{align}
  \partial_\mu \left[-\ii \frac{\partial \mathcal{L}}{\partial \phi_{,\mu}} \phi + \ii \frac{\partial \mathcal{L}}{\partial \phi_{,\mu}^\ast} \phi^\ast \right] = 0 \label{eq:327}
\end{align}
Daraus lässt sich die folgende integrale Erhaltungsgröße gewinnen
\begin{align}
  Q &= \frac{\ii}{\hbar} \int \diff^3 \bm{r} \left(\phi^\ast \pi^\ast - \phi \pi \right), \label{eq:328}
\end{align}
die Ladung heißt.
Tatsächlich, wenn man Gleichung~\eqref{eq:327} explizit mit
Gleichung~\eqref{eq:35a} ausrechnet, findet man
\begin{subequations}
  \begin{align}
    \partial_\mu j^\mu &= 0 \label{eq:329a}
    \intertext{mit}
    j^\mu &= \frac{\ii \hbar }{2m} \left(\phi^\ast \partial^\mu \phi - \phi \partial^\mu \phi^\ast \right) \label{eq:329b}
  \end{align}
\end{subequations}
die nicht positiv definite Wahrscheinlichkeitsdichte $j^0$ und
Stromdichte $\bm{j}$ des Klein-Gordon-Feldes. Diese hat bereits bei der Diskussion der Klein-Gordon-Gleichung Probleme in der Interpretation bereitet.
Für die elektrische Ladung lässt sich diese Erhaltungsgröße aber
sehr gut verstehen (Ladung $q$ des Teilchens),
\begin{subequations}
  \begin{align}
    Q_{q} &= \frac{\ii q}{\hbar} \int \diff^3 \bm{r} \left(\phi^\ast \pi^\ast - \phi \pi \right) \mpunkt \label{eq:330a} 
    \intertext{Die dazugehörigen Operatoren lauten}
    \hat{Q}_q &= \frac{\ii q}{\hbar} \int \diff^3 \bm{r} (\hat{\phi}^\dagger \hat{\pi}^\dagger -  \hat{\phi} \hat{\pi}) \label{eq:330b} \\
    \nOrd{\hat{Q}_q} &= q \int \diff^3 \bm{k} \left( \hat{n}_{a,\bm{k}} - \hat{n}_{c,\bm{k}} \right) \label{eq:330c} 
  \end{align}
\end{subequations}


Daraus lesen wir ab:
\begin{itemize}
\item Die $a$-Teilchen haben die Ladung $q$
\item Die $c$-Teilchen beschreiben die zugehörigen Antiteilchen mit
  Ladung $-q$ (sonst identisch, positive Energien)
\item Ist das Feld reell, kann es nach~\eqref{eq:330a} nur Teilchen
  der Ladung Null beschreiben, d.h.\ Teilchen sind gleich ihre
  Antiteilchen (\acct{Majorana-Teilchen}).
\item Komplexe Felder sind in der Lage, die geladenen $\pi^+$ und
  $\pi^-$-Mesonen zu beschreiben.
\item Aber auch $K^0$ und $\overline{K}^0$, die nicht elektrisch
  geladen sind, werden durch komplexe Felder
  beschreiben. Sie unterscheiden sich in der Hyperladung $Q$ nach
  Gleichung~\eqref{eq:330a}. Sie sind also keine Majorana-Teilchen.
\end{itemize}

\paragraph{e) Impuls:} Eine weitere Erhaltung folgt aus der Translationsinvarianz aus~\eqref{eq:35a}.
\begin{align}
  \diff \phi = 0\quad,\quad \diff x^\ell = \varepsilon^\ell
\end{align}
Daraus
\begin{subequations}
  \label{eq:332}
  \begin{align}
    \bm{p} &= - \int \diff^3 \bm{r} \left( \pi \nabla \phi + \pi^\ast
      \nabla \phi^\ast \right) \label{eq:332a} 
    \intertext{bzw.\ die Operatoren dazu} 
    \bm{p} &= - \int \diff^3 \bm{r} \left(
      \pi \nabla \phi + \pi^\dagger \nabla
      \phi^\dagger \right) \mpunkt \label{eq:332b} 
    \intertext{Dies
      können wir als Impuls verstehen, denn tatsächlich folgt für das
      Klein-Gordon-Feld}
    \nOrd{\hat{p}} &= \int \diff^3 \bm{k} \, \hbar \bm{k} \left( \hat{n}_{a,\bm{k}} + \hat{n}_{c,\bm{k}}
    \right). \label{eq:332c}
  \end{align}
\end{subequations}


