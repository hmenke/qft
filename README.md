# LICENSE

Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2014-2015
Universität Stuttgart.

Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen
Bedingungen 3.0 Deutschland zugänglich. Um eine Kopie dieser Lizenz
einzusehen, konsultieren Sie
http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie
sich brieflich an Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.

# COMPILING

Compile `main.tex` by using:

    make all

If you are editing the source code and you don't want the Makefile to
rebuild the index and the bibliography use

    make quick

or setup your favourite LaTeX-Editor and use your keyboard shortcuts.

Building in dvi-mode will fail, due to the microtype package.

Building was tested with TeXlive 2014.